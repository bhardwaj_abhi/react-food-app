import gallery_1 from "../../assets/images/gallery_1.jpg";
import gallery_2 from "../../assets/images/gallery_2.jpg";
import gallery_3 from "../../assets/images/gallery_3.jpg";
import gallery_4 from "../../assets/images/gallery_4.jpg";
import gallery_6 from "../../assets/images/gallery_6.jpg";
import gallery_5 from "../../assets/images/gallery_5.jpg";
import gallery_7 from "../../assets/images/gallery_7.jpg";
import gallery_8 from "../../assets/images/gallery_8.jpg";
import { ADD_TO_CART, REMOVE_ITEM, SUB_QUANTITY, ADD_QUANTITY, CLEAR_CART } from '../actions/action-types/cart-actions'

const cartitems = localStorage.getItem('cartItems') ? JSON.parse(localStorage.getItem('cartItems')) : [];
const carttotal = localStorage.getItem('carttotal') ? parseFloat(localStorage.getItem('carttotal')) : 0;
const initState = {
  items: [
    { id: 1, name: 'Bake burger Pizza', price: '40', desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nihil cupiditate ut vero alias quaerat inventore molestias vel suscipit explicabo.', img: gallery_1 },
    { id: 2, name: 'Salted Fried Chicken', price: '20', desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nihil cupiditate ut vero alias quaerat inventore molestias vel suscipit explicabo.', img: gallery_2 },
    { id: 3, name: 'Italian Sauce Mushroom', price: '10', desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nihil cupiditate ut vero alias quaerat inventore molestias vel suscipit explicabo.', img: gallery_3 },
    { id: 4, name: 'Fried Potato w/ Garlic', price: '5', desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nihil cupiditate ut vero alias quaerat inventore molestias vel suscipit explicabo.', img: gallery_4 },
    { id: 5, name: 'Bake Sandwich Pizza', price: '50', desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nihil cupiditate ut vero alias quaerat inventore molestias vel suscipit explicabo.', img: gallery_5 },
    { id: 6, name: 'Bake Muffin', price: '25', desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nihil cupiditate ut vero alias quaerat inventore molestias vel suscipit explicabo.', img: gallery_6 },
    { id: 7, name: 'Bake Muffin Pizza', price: '15', desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nihil cupiditate ut vero alias quaerat inventore molestias vel suscipit explicabo.', img: gallery_7 },
    { id: 8, name: 'Bake Potato Pizza', price: '70', desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nihil cupiditate ut vero alias quaerat inventore molestias vel suscipit explicabo.', img: gallery_8 },
  ],
  addedItems: cartitems,
  total: carttotal

}
const cartReducer = (state = initState, action) => {

  //INSIDE HOME COMPONENT
  switch (action.type) {
    case ADD_TO_CART: {
      let addedItem = state.items.find(item => item.id === action.id);
      //check if the action id exists in the addedItems
      let existed_item = state.addedItems.find(item => action.id === item.id);
      if (existed_item) {
        addedItem.quantity += 1;
        let newTotal = state.total + parseFloat(addedItem.price);

        localStorage.setItem('cartItems', JSON.stringify(addedItem));
        localStorage.setItem('carttotal', newTotal);

        return {
          ...state,
          total: newTotal
        }
      }
      else {
        addedItem.quantity = 1;
        //calculating the total
        let newTotal = state.total + parseFloat(addedItem.price)
        localStorage.setItem('cartItems', JSON.stringify([...state.addedItems, addedItem]));
        localStorage.setItem('carttotal', newTotal);

        return {
          ...state,
          addedItems: [...state.addedItems, addedItem],
          total: newTotal
        }

      }
    }
    case REMOVE_ITEM: {
      let itemToRemove = state.addedItems.find(item => action.id === item.id)
      let new_items = state.addedItems.filter(item => action.id !== item.id)

      //calculating the total
      let newTotal = state.total - (parseFloat(itemToRemove.price) * itemToRemove.quantity)
      localStorage.setItem('cartItems', JSON.stringify(new_items));
      localStorage.setItem('carttotal', newTotal);

      return {
        ...state,
        addedItems: new_items,
        total: newTotal
      }
    }
    //INSIDE CART COMPONENT
    case ADD_QUANTITY: {
      let addedItem = state.items.find(item => item.id === action.id)
      addedItem.quantity += 1
      let newTotal = state.total + parseFloat(addedItem.price);
      localStorage.setItem('cartItems', JSON.stringify(addedItem));
      localStorage.setItem('carttotal', newTotal);

      return {
        ...state,
        total: newTotal
      }
    }
    case SUB_QUANTITY: {
      let addedItem = state.items.find(item => item.id === action.id)
      //if the qt == 0 then it should be removed
      if (addedItem.quantity === 1) {
        let new_items = state.addedItems.filter(item => item.id !== action.id)
        let newTotal = state.total - parseFloat(addedItem.price);
        localStorage.setItem('cartItems', JSON.stringify(new_items));
        localStorage.setItem('carttotal', newTotal);

        return {
          ...state,
          addedItems: new_items,
          total: newTotal
        }
      }
      else {
        addedItem.quantity -= 1
        let newTotal = state.total - parseFloat(addedItem.price)
        localStorage.setItem('cartItems', JSON.stringify(addedItem));
        localStorage.setItem('carttotal', newTotal);

        return {
          ...state,
          total: newTotal
        }
      }

    }
    case CLEAR_CART: {
      localStorage.removeItem('cartItems');
      localStorage.removeItem('carttotal');

      return {
        ...state,
        addedItems: [],
        total: 0
      }
    }

    default: {
      return state
    }
  }

}

export default cartReducer
