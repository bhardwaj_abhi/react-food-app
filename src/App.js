import './App.css';
import Header from './components/header';
import { BrowserRouter, Switch, Redirect, Route } from 'react-router-dom';
import Footer from './components/footer';
import Home from './components/home';
import Reservation from './components/reservation';
import OrderView from './components/orderview';
import Menu from './components/menu';
import ReservationSuccess from './components/reservation-success';
import PaymentSuccess from './components/payment-success';
import PayOrder from './components/pay-order';
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Switch>

          <Route path="/home">
            <Home />
          </Route>
          <Route path="/view-order">
            <OrderView />
          </Route>
          <Route path="/reservation">
            <Reservation />
          </Route>
          <Route path="/reservation-success">
            <ReservationSuccess />
          </Route>
          <Route path="/payment-success">
            <PaymentSuccess />
          </Route>
          <Route path="/pay-order">
            <PayOrder />
          </Route>
          <Route path="/menu">
            <Menu />
          </Route>
          <Redirect from="/" to="home" />
        </Switch>
      </BrowserRouter>
      <Footer />

    </div>
  );
}

export default App;
