import gallery_2 from '../assets/images/gallery_2.jpg'
import hero_1 from "../assets/images/hero_1.jpg";
const OrderSuccess = ({ }) => {
    return (
        <div>

            <header id="fh5co-header" className="fh5co-cover js-fullheight" role="banner" style={{ backgroundImage: `url(${hero_1})`, height: 100 + 'vh', backgroundPosition: '0px 0px' }}  >
                <div className="overlay"></div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 text-center">
                            <div className="display-t js-fullheight" style={{ height: 100 + 'vh' }} >
                                <div className="display-tc js-fullheight animate-box" data-animate-effect="fadeIn">
                                    <h1>Order View</h1>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div className="order-section">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-4">
                            <div className="order-preview-img">
                                <img src={gallery_2} alt="Order Preview Image" />
                            </div>
                        </div>
                        <div className="col-sm-8">
                            <div className="product-content">
                                <h1 itemprop="name" className="product-title">
                                    Italian Sauce Mushroom</h1>
                                <div className="price-sku-brand-container">
                                    <div className="product-sku">
                                        <span className="sku-text">Sku :</span>
                                        <span itemprop="sku" className="sku"> Italian Sauce Mushroom </span>
                                    </div>
                                    <div itemprop="offers"
                                        className="product-price-container">
                                        <span className="price-title">Price: </span>
                                        <span className="sale-price">
                                            17.99
                                    </span>
                                        <meta itemprop="price" content="100.00000" />
                                        <meta itemprop="priceCurrency" content="USD" />

                                    </div>
                                    <div className="category-title">
                                        <span className="cat-title">Category: </span>
                                        <span className="cat-name"><a
                                            href="/yummy/index.php/gallery?filter_catid=12">Starter</a></span>
                                    </div>
                                    <p><a href="pay-order.html" className="btn btn-primary btn-outline automation-pay-order">Pay Now
                                    </a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default OrderSuccess