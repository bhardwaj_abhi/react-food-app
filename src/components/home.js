import hero_1 from "../assets/images/hero_1.jpg";
import gallery_9 from "../assets/images/gallery_9.jpg";
import gallery_8 from "../assets/images/gallery_8.jpg";
import gallery_7 from "../assets/images/gallery_7.jpg";
import gallery_6 from "../assets/images/gallery_6.jpg";
import person_1 from "../assets/images/person_1.jpg";

const Home = ({ }) => {
    return (
        <div>
            <header id="fh5co-header" className="fh5co-cover js-fullheight" role="banner" style={{ backgroundImage: `url(${hero_1})`, height: 100 + 'vh', backgroundPosition: '0px 0px' }}  >
                <div className="overlay"></div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 text-center">
                            <div className="display-t js-fullheight" style={{ height: 100 + 'vh' }} >
                                <div className="display-tc js-fullheight animate-box" data-animate-effect="fadeIn">
                                    <h1>The Best Coffee <em>&amp;</em> Restaurant <em>in</em> Brooklyn</h1>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div id="fh5co-about" className="fh5co-section">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6 col-md-pull-4 img-wrap animate-box" data-animate-effect="fadeInLeft">
                            <img src={hero_1} alt="Free Restaurant Bootstrap Website Template by FreeHTML5.co" />
                        </div>
                        <div className="col-md-5 col-md-push-1 animate-box">
                            <div className="section-heading">
                                <h2>The Restaurant</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae neque quisquam at deserunt ab praesentium architecto tempore saepe animi voluptatem molestias, eveniet aut laudantium alias, laboriosam excepturi, et numquam? Atque tempore iure tenetur perspiciatis, aliquam, asperiores aut odio accusamus, unde libero dignissimos quod aliquid neque et illo vero nesciunt. Sunt!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam iure reprehenderit nihil nobis laboriosam beatae assumenda tempore, magni ducimus abentey.</p>
                                <p><a href="#" className="btn btn-primary btn-outline">Our History</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="fh5co-featured-menu" className="fh5co-section">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 fh5co-heading animate-box">
                            <h2>Today's Menu</h2>
                            <div className="row">
                                <div className="col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ab debitis sit itaque totam, a maiores nihil, nulla magnam porro minima officiis! Doloribus aliquam voluptates corporis et tempora consequuntur ipsam, itaque, nesciunt similique commodi omnis. Ad magni perspiciatis, voluptatum repellat.</p>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-item-wrap animate-box">
                            <div className="fh5co-item">
                                <img src={gallery_9} className="img-responsive" alt="Free Restaurant Bootstrap Website Template by FreeHTML5.co" />
                                <h3>Bake Potato Pizza</h3>
                                <span className="fh5co-price">$20<sup>.50</sup></span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nihil cupiditate ut vero alias quaerat inventore molestias vel suscipit explicabo.</p>
                            </div>
                        </div>
                        <div className="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-item-wrap animate-box">
                            <div className="fh5co-item margin_top">
                                <img src={gallery_8} className="img-responsive" alt="Free Restaurant Bootstrap Website Template by FreeHTML5.co" />
                                <h3>Salted Fried Chicken</h3>
                                <span className="fh5co-price">$19<sup>.00</sup></span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nihil cupiditate ut vero alias quaerat inventore molestias vel suscipit explicabo.</p>
                            </div>
                        </div>
                        <div className="clearfix visible-sm-block visible-xs-block"></div>
                        <div className="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-item-wrap animate-box">
                            <div className="fh5co-item">
                                <img src={gallery_7} className="img-responsive" alt="Free Restaurant Bootstrap Website Template by FreeHTML5.co" />
                                <h3>Italian Sauce Mushroom</h3>
                                <span className="fh5co-price">$17<sup>.99</sup></span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nihil cupiditate ut vero alias quaerat inventore molestias vel suscipit explicabo.</p>
                            </div>
                        </div>
                        <div className="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-item-wrap animate-box">
                            <div className="fh5co-item margin_top">
                                <img src={gallery_6} className="img-responsive" alt="Free Restaurant Bootstrap Website Template by FreeHTML5.co" />
                                <h3>Fried Potato w/ Garlic</h3>
                                <span className="fh5co-price">$22<sup>.50</sup></span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos nihil cupiditate ut vero alias quaerat inventore molestias vel suscipit explicabo.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="fh5co-featured-testimony" className="fh5co-section">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 fh5co-heading animate-box">
                            <h2>Testimony</h2>
                            <div className="row">
                                <div className="col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ab debitis sit itaque totam, a maiores nihil, nulla magnam porro minima officiis! Doloribus aliquam voluptates corporis et tempora consequuntur ipsam, itaque, nesciunt similique commodi omnis.</p>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-5 animate-box img-to-responsive animate-box" data-animate-effect="fadeInLeft">
                            <img src={person_1} alt="" />
                        </div>
                        <div className="col-md-7 animate-box" data-animate-effect="fadeInRight">
                            <blockquote>
                                <p> &ldquo; Quantum ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ab debitis sit itaque totam, a maiores nihil, nulla magnam porro minima officiis! Doloribus aliquam voluptates corporis et tempora consequuntur ipsam. &rdquo;</p>
                                <p className="author"><cite>&mdash; Jane Smith</cite></p>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}
export default Home