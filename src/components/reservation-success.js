import person_1 from "../assets/images/person_1.jpg";
import hero_1 from "../assets/images/hero_1.jpg";
const ReservationSuccess = ({ }) => {
    return (
        <div>

            <header id="fh5co-header" className="fh5co-cover js-fullheight" role="banner" style={{ backgroundImage: `url(${hero_1})`, height: 100 + 'vh', backgroundPosition: '0px 0px' }}  >
                <div className="overlay"></div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 text-center">
                            <div className="display-t js-fullheight" style={{ height: 100 + 'vh' }} >
                                <div className="display-tc js-fullheight animate-box" data-animate-effect="fadeIn">
                                    <h1>Your table is reserved successfully!</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div id="fh5co-reservation-form" className="fh5co-section">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 fh5co-heading animate-box">
                            <h2>Thankyou for visiting our site</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div id="fh5co-featured-testimony" className="fh5co-section">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 fh5co-heading animate-box">
                            <h2>Testimony</h2>
                            <div className="row">
                                <div className="col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ab debitis sit itaque totam, a maiores nihil, nulla magnam porro minima officiis! Doloribus aliquam voluptates corporis et tempora consequuntur ipsam, itaque, nesciunt similique commodi omnis.</p>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-5 animate-box img-to-responsive animate-box" data-animate-effect="fadeInLeft">
                            <img src={person_1} alt="" />
                        </div>
                        <div className="col-md-7 animate-box" data-animate-effect="fadeInRight">
                            <blockquote>
                                <p> &ldquo; Quantum ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ab debitis sit itaque totam, a maiores nihil, nulla magnam porro minima officiis! Doloribus aliquam voluptates corporis et tempora consequuntur ipsam. &rdquo;</p>
                                <p className="author"><cite>&mdash; Jane Smith</cite></p>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    )
}
export default ReservationSuccess