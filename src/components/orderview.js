import { useEffect } from 'react';
import { useHistory } from 'react-router';
import hero_1 from "../assets/images/hero_1.jpg";
import './order-view.css';
import { removeItem, addQuantity, subtractQuantity } from '../redux/actions/cartActions'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

const OrderView = ({ orders, addQuantity, subtractQuantity, removeItem }) => {
    const history = useHistory();
    const handleRemove = (id) => {
        removeItem(id);
    }
    //to add the quantity
    const handleAddQuantity = (id) => {
        addQuantity(id);
    }
    //to substruct from the quantity
    const handleSubtractQuantity = (id) => {
        subtractQuantity(id);
    }
    useEffect(() => {
        if (!orders) {
            history.push('/menu')
        }
    }, []);
    const payNow = () => {
        history.push('/pay-order')
    }
    return (
        <div>

            <header id="fh5co-header" className="fh5co-cover js-fullheight" role="banner" style={{ backgroundImage: `url(${hero_1})`, height: 100 + 'vh', backgroundPosition: '0px 0px' }}  >
                <div className="overlay"></div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 text-center">
                            <div className="display-t js-fullheight" style={{ height: 100 + 'vh' }} >
                                <div className="display-tc js-fullheight animate-box" data-animate-effect="fadeIn">
                                    <h1>Order View</h1>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div className="order-section">
                <div className="container">
                    {orders.length ? (
                        <div>
                            { orders.map((order) => (
                                <div key={order.id} className="row">
                                    <div className="col-sm-4">
                                        <div className="order-preview-img">
                                            <img src={order.img} alt="Order Preview" />
                                        </div>
                                    </div>
                                    <div className="col-sm-8">
                                        <div className="product-content">
                                            <h1 itemprop="name" className="product-title">
                                                {order.name}</h1>
                                            <div className="price-sku-brand-container">
                                                <div className="product-sku">
                                                    <span className="sku-text">Sku :</span>
                                                    <span itemprop="sku" className="sku"> {order.desc} </span>
                                                </div>
                                                <div itemprop="offers"
                                                    className="product-price-container">
                                                    <span className="price-title">Price: </span>
                                                    <span className="sale-price">
                                                        ${order.price}
                                                    </span>
                                                    <meta itemprop="price" content="100.00000" />
                                                    <meta itemprop="priceCurrency" content="USD" />

                                                </div>
                                                <div className="category-title">
                                                    <span className="cat-title">Category: </span>
                                                    <span className="cat-name"><a
                                                        href="/yummy/index.php/gallery?filter_catid=12">Starter</a></span>
                                                </div>
                                                <div className="add-remove">
                                                    <p>
                                                        <Link className="btn btn-primary btn-outline automation-pay-order" to="/view-order"><i className="material-icons" onClick={() => { handleAddQuantity(order.id) }}>+</i></Link>

                                                        <b>Quantity: {order.quantity}</b>
                                                        <Link className="btn btn-primary btn-outline automation-pay-order" to="/view-order"><i className="material-icons" onClick={() => { handleSubtractQuantity(order.id) }}>-</i></Link>

                                                    </p>
                                                </div>
                                                <button className="btn btn-primary btn-outline automation-pay-order" onClick={() => { handleRemove(order.id) }}>Remove</button>




                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))}
                            < p className="p-20">
                                <a onClick={payNow} className="btn btn-primary btn-block btn-lg btn-outline automation-pay-order">Pay Now </a>
                            </p>
                        </div>
                    ) :
                        (
                            <p>Please add some products.</p>
                        )
                    }

                </div>
            </div>
        </div >
    )
}


const mapStateToProps = (state) => ({
    orders: state.addedItems,
});
const mapDispatchToProps = (dispatch) => ({
    removeItem: (id) => { dispatch(removeItem(id)) },
    addQuantity: (id) => { dispatch(addQuantity(id)) },
    subtractQuantity: (id) => { dispatch(subtractQuantity(id)) }
});
export default connect(mapStateToProps, mapDispatchToProps)(OrderView)