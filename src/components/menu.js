import hero_1 from "../assets/images/hero_1.jpg";
import person_1 from "../assets/images/person_1.jpg";

import { useHistory } from "react-router";
import { connect } from "react-redux";
import { addToCart } from '../redux/actions/cartActions'

const Menu = ({ menuData, addToCart }) => {
    const history = useHistory();
    const orderNow = (order) => {
        addToCart(order.id);
        history.push('/view-order');
    }
    return (
        <div>
            <header id="fh5co-header" className="fh5co-cover js-fullheight" role="banner" style={{ backgroundImage: `url(${hero_1})`, height: 100 + 'vh', backgroundPosition: '0px 0px' }}  >
                <div className="overlay"></div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 text-center">
                            <div className="display-t js-fullheight" style={{ height: 100 + 'vh' }} >
                                <div className="display-tc js-fullheight animate-box" data-animate-effect="fadeIn">
                                    <h1>See <em>Our</em> Menu </h1>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <div id="fh5co-featured-menu" className="fh5co-section">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 fh5co-heading animate-box">
                            <h2>Our Delicous Menu</h2>
                            <div className="row">
                                <div className="col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ab debitis sit itaque totam, a maiores nihil, nulla magnam porro minima officiis! Doloribus aliquam voluptates corporis et tempora consequuntur ipsam, itaque, nesciunt similique commodi omnis. Ad magni perspiciatis, voluptatum repellat.</p>
                                </div>
                            </div>
                        </div>
                        {
                            menuData.map((item) => (
                                <div key={item.id} className="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-item-wrap">
                                    <div className="automation-fh5co-item animate-box">
                                        <img src={item.img} className="img-responsive" alt="Free Restaurant Bootstrap Website Template by FreeHTML5.co" />
                                        <h3>{item.name}</h3>
                                        <span className="fh5co-price">{item.price}</span>
                                        <p>{item.desc}</p>
                                        <p><a id="bpp" className="btn btn-primary btn-outline" onClick={() => { orderNow(item) }} >Order Now</a></p>
                                    </div>
                                </div>
                            ))
                        }

                    </div>
                </div>
            </div>
            <div id="fh5co-featured-testimony" className="fh5co-section">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 fh5co-heading animate-box">
                            <h2>Testimony</h2>
                            <div className="row">
                                <div className="col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ab debitis sit itaque totam, a maiores nihil, nulla magnam porro minima officiis! Doloribus aliquam voluptates corporis et tempora consequuntur ipsam, itaque, nesciunt similique commodi omnis.</p>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-5 animate-box img-to-responsive animate-box" data-animate-effect="fadeInLeft">
                            <img src={person_1} alt="" />
                        </div>
                        <div className="col-md-7 animate-box" data-animate-effect="fadeInRight">
                            <blockquote>
                                <p> &ldquo; Quantum ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ab debitis sit itaque totam, a maiores nihil, nulla magnam porro minima officiis! Doloribus aliquam voluptates corporis et tempora consequuntur ipsam. &rdquo;</p>
                                <p className="author"><cite>&mdash; Jane Smith</cite></p>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>

        </div >
    )
}

const mapStateToProps = (state) => ({
    menuData: state.items
});
const mapDispatchToProps = (dispatch) => ({
    addToCart: (id) => { dispatch(addToCart(id)) }
});
export default connect(mapStateToProps, mapDispatchToProps)(Menu)