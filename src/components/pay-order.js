import { useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router";
import hero_1 from "../assets/images/hero_1.jpg";
import { clearCart } from '../redux/actions/cartActions';

const PayOrder = ({ amount, clearCart }) => {
    const [email, setEmail] = useState('');
    const [address, setAddress] = useState('');
    const [phone, setPhone] = useState('');
    const [method, setMethod] = useState('credit');
    const history = useHistory();

    const handleOnSubmit = (event) => {
        event.preventDefault();
        if (!email || !address || !phone || !amount || !method) {
            alert('Please fill all fields');
            return;
        }
        clearCart();
        history.push('/payment-success');
    };
    const cancelPay = () => {
        history.push('/view-order')
    }
    return (
        <div>

            <header id="fh5co-header" className="fh5co-cover js-fullheight" role="banner" style={{ backgroundImage: `url(${hero_1})`, height: 100 + 'vh', backgroundPosition: '0px 0px' }}  >
                <div className="overlay"></div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 text-center">
                            <div className="display-t js-fullheight" style={{ height: 100 + 'vh' }} >
                                <div className="display-tc js-fullheight animate-box" data-animate-effect="fadeIn">
                                    <h1>Pay Order</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div className="pay-order">
                <div className="container">
                    <form noValidate name="loginForm" onSubmit={handleOnSubmit}>
                        <div className="form-group">
                            <label >Email address</label>
                            <input type="email" className="form-control automation-exampleInputEmail1" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="email" value={email}
                                onChange={(e) => setEmail(e.target.value)} />
                        </div>
                        <div className="form-group">
                            <label>Address</label>
                            <input type="text" className="form-control automation-exampleAddress" id="exampleAddress" placeholder="Enter Address" name="address" value={address}
                                onChange={(e) => setAddress(e.target.value)} />
                        </div>
                        <div className="form-group">
                            <label >Phone Number</label>
                            <input type="number" className="form-control automation-exampleInputPhone" id="exampleInputPhone" placeholder="Enter Phone Number" name="phone" value={phone}
                                onChange={(e) => setPhone(e.target.value)} />
                        </div>
                        <div className="form-group">
                            <label >Amount</label>
                            <input type="number" className="form-control automation-amount" id="amount" placeholder="Enter Amount" name="email" value={amount} readOnly />
                        </div>
                        <div className="form-group">
                            <label >Enter Payment method</label>
                            <select className="automation-paymentMethod" id="automation-paymentMethod" placeholder="Enter Payment Methods" name="email" value={method}
                                onChange={(e) => setMethod(e.target.value)}>
                                <option value="credit">Credit Card</option>
                                <option value="debit">Debit Card</option>
                                <option value="banking">Net Banking</option>
                                <option value="upi">UPI</option>
                            </select>
                        </div>
                        <p><button id="automation-pay" className="btn btn-primary btn-outline automation-pay" type="submit">Pay Now
                    </button></p>
                        <p><a onClick={cancelPay} className="btn btn-primary btn-outline">Cancel Order
                    </a></p>
                    </form>
                </div>
            </div>

        </div>
    )
}

const mapStateToProps = (state) => ({
    amount: state.total,
});
const mapDispatchToProps = (dispatch) => ({
    clearCart: () => { dispatch(clearCart()) }
});
export default connect(mapStateToProps, mapDispatchToProps)(PayOrder)