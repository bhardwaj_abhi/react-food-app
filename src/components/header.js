import { useState } from "react";
import { Link } from "react-router-dom"

const Header = () => {
    const [path, setPath] = useState('/home');

    return (
        <nav className="fh5co-nav" role="navigation">
            <div className="container">
                <div className="row">
                    <div className="col-xs-12 text-center logo-wrap">
                        <div id="fh5co-logo"><a href="index.html">Tasty<span>.</span></a></div>
                    </div>
                    <div className="col-xs-12 text-center menu-1 menu-wrap">
                        <ul>
                            <li onClick={() => { setPath('/home'); window.scrollTo({ top: 0, }) }} className={path === '/home' ? 'active' : ''}><Link to="/home" >Home</Link></li>
                            <li onClick={() => { setPath('/menu'); window.scrollTo({ top: 0, }) }} className={path === '/menu' ? 'active' : ''}><Link to="/menu" >Menu</Link></li>
                            <li onClick={() => { setPath('/view-order'); window.scrollTo({ top: 0, }) }} className={path === '/view-order' ? 'active' : ''}><Link to="/view-order" >View Order</Link></li>
                            {/* <li><Link to="" >Gallery</Link>
                            </li> */}
                            <li onClick={() => { setPath('/reservation'); window.scrollTo({ top: 0, }) }} className={path === '/reservation' ? 'active' : ''}><Link to="/reservation" >Reservation</Link>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </nav>
    )

}
export default Header