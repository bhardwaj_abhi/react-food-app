const Footer = () => {
  return (
    <footer id="fh5co-footer" role="contentinfo" className="fh5co-section">
      <div className="container">
        <div className="row row-pb-md">
          <div className="col-md-4 fh5co-widget">
            <h4>Tasty</h4>
            <p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit. Eos cumque dicta adipisci architecto culpa amet.</p>
          </div>
          <div className="col-md-2 col-md-push-1 fh5co-widget">
            <h4>Links</h4>
            <ul className="fh5co-footer-links">
              <li><a href="javascript:void(0)">Home</a></li>
              <li><a href="javascript:void(0)">About</a></li>
              <li><a href="javascript:void(0)">Menu</a></li>
              <li><a href="javascript:void(0)">Gallery</a></li>
            </ul>
          </div>

          <div className="col-md-2 col-md-push-1 fh5co-widget">
            <h4>Categories</h4>
            <ul className="fh5co-footer-links">
              <li><a href="javascript:void(0)">Landing Page</a></li>
              <li><a href="javascript:void(0)">Real Estate</a></li>
              <li><a href="javascript:void(0)">Personal</a></li>
              <li><a href="javascript:void(0)">Business</a></li>
              <li><a href="javascript:void(0)">e-Commerce</a></li>
            </ul>
          </div>

          <div className="col-md-4 col-md-push-1 fh5co-widget">
            <h4>Contact Information</h4>
            <ul className="fh5co-footer-links">

            </ul>
          </div>

        </div>

        <div className="row copyright">
          <div className="col-md-12 text-center">
            <ul className="fh5co-social-icons">
              <li><a href="javascript:void(0)"><i className="icon-twitter2"></i></a></li>
              <li><a href="javascript:void(0)"><i className="icon-facebook2"></i></a></li>
              <li><a href="javascript:void(0)"><i className="icon-linkedin2"></i></a></li>
              <li><a href="javascript:void(0)"><i className="icon-dribbble2"></i></a></li>
            </ul>
          </div>
        </div>

      </div>
    </footer>
  )
}
export default Footer