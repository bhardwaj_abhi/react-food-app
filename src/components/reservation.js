import { useState } from "react";
import { useHistory } from "react-router";
import hero_1 from "../assets/images/hero_1.jpg";
import person_1 from "../assets/images/person_1.jpg";
const Reservation = () => {
  const [name, setName] = useState('');
  const [people, setPeople] = useState('1');
  const [phone, setPhone] = useState('');
  const history = useHistory();

  const handleOnSubmit = (event) => {
    event.preventDefault();
    if (!name || !people || !phone || !phone) {
      alert('Please fill all fields');
      return;
    }
    localStorage.removeItem('order-details');
    history.push('/reservation-success');
  };
  return (
    <div>

      <header id="fh5co-header" className="fh5co-cover js-fullheight" role="banner" style={{ backgroundImage: `url(${hero_1})`, height: 100 + 'vh', backgroundPosition: '0px 0px' }}  >
        <div className="overlay"></div>
        <div className="container">
          <div className="row">
            <div className="col-md-12 text-center">
              <div className="display-t js-fullheight" style={{ height: 100 + 'vh' }} >
                <div className="display-tc js-fullheight animate-box" data-animate-effect="fadeIn">
                  <h1>Reserved a Table Today!</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>

      <div id="fh5co-reservation-form" className="fh5co-section">
        <div className="container">
          <div className="row">
            <div className="col-md-12 fh5co-heading animate-box">
              <h2>Reservation</h2>
              <div className="row">
                <div className="col-md-6">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ab debitis sit itaque totam, a maiores nihil, nulla magnam porro minima officiis! Doloribus aliquam voluptates corporis et tempora consequuntur ipsam, itaque, nesciunt similique commodi omnis. Ad magni perspiciatis, voluptatum repellat.</p>
                </div>
              </div>
            </div>

            <div className="col-md-6 col-md-push-6 col-sm-6 col-sm-push-6">
              <form noValidate onSubmit={handleOnSubmit} id="form-wrap">
                <div className="row form-group">
                  <div className="col-md-12">
                    <label >Your Name</label>
                    <input type="text"
                      name="name"
                      value={name}
                      onChange={(e) => setName(e.target.value)} className="form-control automation-name" id="automation-name" />
                  </div>
                </div>
                <div className="row form-group">
                  <div className="col-md-12">
                    <label >How Many People</label>
                    <select
                      name="people"
                      value={people}
                      onChange={(e) => setPeople(e.target.value)} className="form-control automation-custom_select">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4+">4+</option>
                    </select>
                  </div>
                </div>
                <div className="row form-group">
                  <div className="col-md-12">
                    <label >When</label>
                    <input type="text" name="phone"
                      value={phone}
                      onChange={(e) => setPhone(e.target.value)}
                      id="automation-taskdatetime" className="form-control automation-taskdatetime" />
                  </div>
                </div>
                <div className="row form-group">
                  <div className="col-md-12">
                    <input type="submit" className="btn btn-primary btn-outline btn-lg" value="Submit Form" />
                  </div>
                </div>

              </form>
            </div>


          </div>
        </div>
      </div>

      <div id="fh5co-featured-testimony" className="fh5co-section">
        <div className="container">
          <div className="row">
            <div className="col-md-12 fh5co-heading animate-box">
              <h2>Testimony</h2>
              <div className="row">
                <div className="col-md-6">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ab debitis sit itaque totam, a maiores nihil, nulla magnam porro minima officiis! Doloribus aliquam voluptates corporis et tempora consequuntur ipsam, itaque, nesciunt similique commodi omnis.</p>
                </div>
              </div>
            </div>

            <div className="col-md-5 animate-box img-to-responsive animate-box" data-animate-effect="fadeInLeft">
              <img src={person_1} alt="" />
            </div>
            <div className="col-md-7 animate-box" data-animate-effect="fadeInRight">
              <blockquote>
                <p> &ldquo; Quantum ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ab debitis sit itaque totam, a maiores nihil, nulla magnam porro minima officiis! Doloribus aliquam voluptates corporis et tempora consequuntur ipsam. &rdquo;</p>
                <p className="author"><cite>&mdash; Jane Smith</cite></p>
              </blockquote>
            </div>
          </div>
        </div>
      </div>

    </div>
  )
}
export default Reservation